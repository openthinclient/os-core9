
"Image" will be the top directory for a bootable image, including kernel, initrd, userland

The source directory for the initrd is "Initrd", and the one for busybox right within "Sources/busybox"

The configs should be put to "Sources" (like kernel-i386.conf for the kernel)


[![Join the chat at https://gitter.im/openthinclient/os-core](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/openthinclient/os-core?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
