#!/usr/bin/make -f

# Makefile for TCOS
#  (C) Jörn Frenzel   <j.frenzel@openthinclient.com> since 2013
#  (C) Marton Morvai  <m.morvai@openthinclient.com>  since 2019
#  (C) Steffen Hoenig <s.hoenig@openthinclient.com>  2013-2015
#
# License: GPL V2
#


# All targets are accessible for user
.PHONY: all

# send all vars to sub processes
.EXPORT_ALL_VARIABLES: ;

# this variable is different from the make build in "shell" - lower case
SHELL := /bin/bash
PATH := /usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:/bin:/sbin

HOST_ARCH := $(shell file /sbin/fdisk)

ifeq (80386,$(findstring 80386,$(HOST_ARCH)))
        HOST_ARCH=i386
endif
ifeq (x86-64,$(findstring x86-64,$(HOST_ARCH)))
        HOST_ARCH=x86_64
endif


DEB_DIST_NAME := stretch
TARGET_ARCH := i386
TARGET_ARCH64 := amd64
TOP_DIR := $(dir $(lastword $(MAKEFILE_LIST)))

BASE_VERSION ?= 2019
#BASE_VERSION_MINOR := dev_$(shell date -I)
BASE_VERSION_MINOR := .1

BUSYBOX_BRANCH = 1_26_stable
FLASH_PLAYER_VERSION ?= 32.0.0.207
MODULES_LIST_CHROOT ?= /TCOS/Sources/modules.list

# Use a local deb mirror to speed up your build process!
DEB_MIRROR ?= http://localhost/debian
#DEB_MIRROR ?= http://http.debian.net/debian

#LOCAL_TEST_PATH ?= root@mm-server1:/home/openthinclient/otc-manager-home/nfs/root/
LOCAL_TEST_PATH ?= root@joern-2019:/home/openthinclient/otc-manager-home/nfs/root/

TARGET_KERNEL_DEFAULT = 4.19.0-0.bpo.5-686-pae
TARGET_KERNEL_NONPAE = 4.19.0-0.bpo.5-686
TARGET_KERNEL_64 = 4.19.0-0.bpo.5-amd64
TARGET_KERNEL = $(TARGET_KERNEL_DEFAULT) $(TARGET_KERNEL_NONPAE)  $(TARGET_KERNEL_64)

# A stupid way to show all packages from a specific repo.
# grep ^Package: /var/lib/apt/lists/http.debian.net_debian_dists_wheezy-backports* | awk '{print $NF}' | sort -u

# sort packages quickly
# for p in one two three; do echo $p; done | sort -u | tr '\n' ' '

#
PACKAGES += alsa-utils apt aptitude apt-utils arandr atril binutils ca-certificates caja cheese cifs-utils clipit console-data console-setup curl coreutils dbus dbus-x11 dconf-tools devilspie devilspie2 dialog dmidecode dnsutils dos2unix dosfstools e2fsprogs efibootmgr eject engrampa eom ethtool file firefox-esr-l10n-de fontconfig gir1.2-gstreamer-1.0 gir1.2-gtk-2.0 gdevilspie gdisk gstreamer1.0-alsa gstreamer1.0-clutter-3.0 gstreamer1.0-crystalhd gstreamer1.0-fluendo-mp3 gstreamer1.0-libav gstreamer1.0-nice gstreamer1.0-plugins-bad gstreamer1.0-plugins-base gstreamer1.0-plugins-base-apps gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly gstreamer1.0-pulseaudio gstreamer1.0-tools gstreamer1.0-vaapi gstreamer1.0-x gtkperf gvfs gvfs-backends gvfs-bin haveged hdparm htop hwinfo iproute2 iputils-ping ipython isc-dhcp-client kbd ldap-utils less libappindicator1 libav-tools libavcodec-extra57 libcurl3 libexo-1-0 libgl1-mesa-dri libgl1-mesa-glx libgssglue1 libgstreamer1.0-dev libiomp5 libjpeg62-turbo libminizip1 libre2-3 libmrm4 libnotify-bin libnspr4 libnss3 libnss3-tools libpam-ldap libpam-script libpcsc-perl libsasl2-modules libsasl2-modules-gssapi-mit libssl1.0.2 libssl1.1 libstdc++5 libthunarx-2-0 libuil4 libuuid-perl libwebkitgtk-1.0-0 libx11-6 libxcb-util0 libxdo3 libxerces-c3.1 libxfce4util-bin libxfce4ui-1-0 libxfce4util7 libxm4 lightdm lightdm-gtk-greeter lshw ltrace man-db marco mate-applets mate-desktop mate-media mate-power-manager mate-screensaver mate-session-manager mate-system-monitor mate-themes mate-utils mc mesa-utils mpv net-tools nfs-common ntfs-3g ntp numlockx nwipe openssh-client openssh-server open-vm-tools parted pavucontrol pciutils pluma pulseaudio pulseaudio-utils python-configparser python-crypto python-gconf python-gtk2 python-ldap python-netifaces python-nss python-psutil python-pyqt5 python-pyqt5.qtwebkit python-qt4 python-requests python-xdg rdesktop read-edid rsync screen snmp snmpd source-highlight squashfs-tools strace sudo syslog-ng tcpdump telnet thunar ttf-dejavu udhcpc unzip usbutils util-linux vim vim-tiny virt-viewer virtualbox-guest-utils virtualbox-guest-x11 wget wmctrl wpasupplicant x11vnc xdg-utils xdotool xfce4-panel xfce4-session xfce4-settings xfdesktop4 xfonts-base xfwm4 xinetd xinput xinput-calibrator xtightvncviewer xvkbd zenity gir1.2-mate-panel gir1.2-matepanelapplet-4.0 apt-rdepends intel-gpu-tools vainfo glx-alternative-mesa glx-diversions radeontop jq iftop fonts-liberation libpam-mount cups-client cups-bsd

# REMEMBER:  xserver-xorg-video-all does not really contain all the xorg-video modules
#
PACKAGES_X += arandr i965-va-driver libdrm2 libdrm-amdgpu1 libdrm-intel1 libdrm-nouveau2 libdrm-radeon1 libvdpau-va-gl1 mesa-vdpau-drivers vdpauinfo vdpau-va-driver x11-xserver-utils xorg xserver-xorg xserver-xorg-core xserver-xorg-input-elographics xserver-xorg-input-evdev xserver-xorg-input-multitouch xserver-xorg-input-mutouch xserver-xorg-input-wacom xserver-xorg-video-all xserver-xorg-video-amdgpu xserver-xorg-video-ati xserver-xorg-video-cirrus xserver-xorg-video-geode xserver-xorg-video-glide xserver-xorg-video-intel xserver-xorg-video-ivtv xserver-xorg-video-mach64 xserver-xorg-video-mga xserver-xorg-video-neomagic xserver-xorg-video-r128 xserver-xorg-video-radeon xserver-xorg-video-savage xserver-xorg-video-sisusb xserver-xorg-video-tdfx xserver-xorg-video-trident xserver-xorg-video-openchrome 


PACKAGES_X_NVIDIA             += xserver-xorg-video-nvidia-legacy-304xx xserver-xorg-video-nvidia-legacy-340xx xserver-xorg-video-nvidia-legacy-390xx
PACKAGES_X_NVIDIA_VDPAU       += nvidia-legacy-304xx-vdpau-driver nvidia-legacy-340xx-vdpau-driver nvidia-legacy-390xx-vdpau-driver
PACKAGES_X_NVIDIA_DRIVER_BIN  += nvidia-legacy-304xx-driver-bin   nvidia-legacy-340xx-driver-bin   nvidia-legacy-390xx-driver-bin
PACKAGES_X_NVIDIA_LIBS_GLX    += libgl1-nvidia-legacy-304xx-glx libgl1-nvidia-legacy-340xx-glx libgl1-nvidia-legacy-390xx-glx
PACKAGES_X_NVIDIA_LIBS_EGL     += libegl1-nvidia-legacy-340xx libegl1-nvidia-legacy-390xx
PACKAGES_X_NVIDIA_LIBS_EGLCORE += libnvidia-legacy-340xx-eglcore libnvidia-legacy-390xx-eglcore 
PACKAGES_X_NVIDIA_LIBS_GLCORE  += libnvidia-legacy-304xx-glcore libnvidia-legacy-340xx-glcore libnvidia-legacy-390xx-glcore 
PACKAGES_X_NVIDIA_LIBS_CFG1    += libnvidia-legacy-304xx-cfg1 libnvidia-legacy-340xx-cfg1 libnvidia-legacy-390xx-cfg1
PACKAGES_X_NVIDIA_ALL += $(PACKAGES_X_NVIDIA) $(PACKAGES_X_NVIDIA_VDPAU) $(PACKAGES_X_NVIDIA_DRIVER_BIN) $(PACKAGES_X_NVIDIA_LIBS_GLX) $(PACKAGES_X_NVIDIA_LIBS_EGL) $(PACKAGES_X_NVIDIA_LIBS_EGLCORE) $(PACKAGES_X_NVIDIA_LIBS_CFG1) $(PACKAGES_X_NVIDIA_LIBS_GLCORE)      

PACKAGES_DKMS  += nvidia-legacy-304xx-kernel-dkms nvidia-legacy-340xx-kernel-dkms nvidia-legacy-390xx-kernel-dkms virtualbox-guest-dkms 

PACKAGES_FIRMWARE += firmware-amd-graphics firmware-crystalhd firmware-intel-sound firmware-linux firmware-linux-nonfree firmware-misc-nonfree firmware-qcom-media firmware-realtek firmware-ti-connectivity 
PACKAGES_FIRMWARE_MISC += firmware-myricom firmware-intelwimax firmware-netronome firmware-netxen firmware-qlogic firmware-samsung firmware-siano  firmware-bnx2 firmware-bnx2x firmware-cavium
PACKAGES_FIRMWARE_WIFI += firmware-b43legacy-installer firmware-b43legacy-installer firmware-iwlwifi firmware-libertas firmware-atheros firmware-brcm80211 firmware-zd1211

# these packages don't work with noninteractive deb frontende, kicked them out
# firmware-ivtv firmware-ipw2x00

PACKAGES_SMARTCARD = libccid libpcsclite1 libacsccid1 libasedrive-usb libgempc430 libasedrive-serial libpcsc-perl pcsc-tools libifd-cyberjack6
PACKAGES_QEMU += qemu-kvm:amd64 vde2:amd64 sgabios ovmf qemu-utils
PACKAGES_DEB += libifd-cyberjack6_3.99.5final.sp12_i386_d09.deb libxp6_1.0.2-2_i386.deb

# This two packages do some autogeneration (triggerd in thier postinstall scripts) of config files in /etc/reader.conf.d/.
# This config files cause pcscd to crash.
# libgempc410 libgcr410

# disable/remove useless services in systemd
MASKED_SERVICES += hwclock.sh alsa-utils nfs-common rpcbind systemd-fsck-root.service systemd-fsck@.service systemd-fsckd.service systemd-fsckd.socket apt-daily-upgrade.service apt-daily.service apt-daily-upgrade.timer apt-daily.timer dev-hugepages.mount dev-mqueue.mount sys-kernel-debug.mount run-rpc_pipefs
DISABLED_SERVICES += haveged snmpd rsync xinetd keyboard-setup console-setup systemd-remount-fs systemd-sysusers wpa_supplicant nfs-client systemd-journal-flush nfs-config 


##
# meta-targets
#

all:  base-release-stamp
test: compressed-stamp upload-test

chroot: filesystem-stamp
	@sudo -E BIND_ROOT=./ Scripts/TCOS.chroot ./Filesystem $(SHELL)

chroot-ro: filesystem-stamp
	@sudo -E AUFS=1 BIND_ROOT=./ Scripts/TCOS.chroot ./Filesystem $(SHELL)
help:
	@echo "[1mWELCOME TO THE TCOS BUILD SYSTEM[0m"
	@echo ""
	@echo "make all		Metatarget: create the whole system"
	@echo ""
	@echo "make filesystem		Bootstrap and fill the TCOS ./Filesystem directory"
	@echo "make busybox		(Re-)Build Busybox"
	@echo "make kernel		(Re-)Build Kernel packages"
	@echo "make initrd		(Re-)Build Initramfs"
	@echo "make chroot		Work inside Filesystem"
	@echo "make chroot-ro		Work inside Filesystem but don't save changes."
	@echo "make update		Install or update required packages in TCOS ./Filesystem"
	@echo "make compressed		Compress base filesystem image."
	@echo
	@echo "Don't worry about the sequence of build commands, this Makefile will tell you"
	@echo "what to do first, in case anything is missing."
	@echo
	@echo "Have a lot of [36mfun[0m. ;-)"

# build-targets
#
filesystem:
	make $@-stamp

filesystem-stamp:
	@echo "[1m Target filesystem-stamp: Creating initial filesystems[0m"
	-rm -f tcosify-stamp update-stamp kernel-stamp clean-stamp
	DEB_DIST_NAME=$(DEB_DIST_NAME) TARGET_ARCH=$(TARGET_ARCH) TARGET_ARCH64=$(TARGET_ARCH64) ./Scripts/TCOS.mkfilesystem $(DEB_MIRROR)
	@touch $@

tcosify:
	make $@-stamp

tcosify-stamp:filesystem-stamp
	@echo "[1m Target tcosify-stamp: Applying TCOS specific changes[0m"
	-rm -f update-stamp kernel-stamp clean-stamp
	Scripts/LINBO.apply-configs Sources/tcos Filesystem
	Scripts/LINBO.apply-configs Sources/tcos Filesystem_dkms_64
	sudo -E Scripts/TCOS.chroot Filesystem 		$(SHELL) < Scripts/TCOS.tcosify-chroot
	sudo -E Scripts/TCOS.chroot Filesystem_dkms_64	$(SHELL) < Scripts/TCOS.tcosify-chroot
	sudo -E Scripts/TCOS.chroot Filesystem          $(SHELL) < Scripts/TCOS.set_apt_sources
	sudo -E Scripts/TCOS.chroot Filesystem_dkms_64  $(SHELL) < Scripts/TCOS.set_apt_sources
	@touch $@

kernel:
	make $@-stamp

kernel-stamp: tcosify-stamp
	@echo "[1m Target kernel-stamp: Install the kernel[0m"
	mkdir -p Base/base-$(BASE_VERSION)
	sudo cp -ra Sources/Base_Skel/* Base/base-$(BASE_VERSION)/
	test -L Base/base-$(BASE_VERSION)/tftp || (cd Base/base-$(BASE_VERSION); ln -snf debian/base/tftp tftp)
	test -L Base/base-$(BASE_VERSION)/sfs  || (cd Base/base-$(BASE_VERSION); ln -snf debian/base/sfs sfs)

	sudo -E BIND_ROOT=./ Scripts/TCOS.chroot Filesystem $(SHELL) -c "\
		set -e ;\
		export PATH=/tmp:$$PATH ;\
		cp /bin/true /tmp/update-initramfs ;\
		apt-get update ;\
		apt-get install -y linux-image-$(TARGET_KERNEL_DEFAULT) linux-image-$(TARGET_KERNEL_NONPAE) ;\
		dpkg --add-architecture amd64; apt-get update ;\
		apt-get install -y linux-image-$(TARGET_KERNEL_64):amd64 ;\
		/etc/init.d/nfs-common stop || true ;\
	"
	sudo cp Filesystem/boot/vmlinuz*  Base/base-$(BASE_VERSION)/tftp/ ; \

	(cd Base/base-$(BASE_VERSION)/tftp/; \
		sudo mv vmlinuz-$(TARGET_KERNEL_DEFAULT) vmlinuz; \
		sudo mv vmlinuz-$(TARGET_KERNEL_NONPAE) vmlinuz_non-pae; \
		sudo mv vmlinuz-$(TARGET_KERNEL_64) vmlinuz_64; \
	)
	@touch $@



update:
	make $@-stamp

update-stamp:kernel-stamp
	@echo "[1m Target $@-stamp [0m"
	-rm -f clean-stamp
	mkdir -p Initrd
	cp Sources/busybox_apply_init/init_functions Initrd
	sudo -E BIND_ROOT=./ Scripts/TCOS.chroot Filesystem $(SHELL) < Scripts/TCOS.update_filesystem

	@echo "[1m Cloning Filesystem to Filesystem_dkms_32[0m"
	sudo rsync -a --exclude="./Filesystem/usr/share" ./Filesystem/ Filesystem_dkms_32/

        # Install certain DEB-Packages
	sudo -E BIND_ROOT=./ Scripts/TCOS.chroot Filesystem $(SHELL) -c "\
                source /TCOS/Initrd/init_functions ;\
                echo \"\$$GREEN############### installing deb-packages ##################### \" \$$NORMAL ;\
                cd /TCOS/Packages && dpkg -i $(PACKAGES_DEB) ;\
	"

        # Install firmware to later picking some of them for initrd.
        # Because we don't know if some module from module.list needs firmware,
        # we just install all firmware packages in Filesystem_dkms_32.
        # The real Filesystem does not contain all firmware packages!
	sudo -E BIND_ROOT=./ Scripts/TCOS.chroot Filesystem_dkms_32 $(SHELL) -c "\
		export DEBIAN_FRONTEND=noninteractive;\
		apt-get update ;\
		apt-get install -y --no-install-recommends \
		$(PACKAGES_FIRMWARE) $(PACKAGES_FIRMWARE_MISC) $(PACKAGES_FIRMWARE_WIFI) ;\
	"
	@touch $@


#nvidia:kernel-stamp
#	@echo "[1m Target $@-stamp [0m"
#	sudo -E BIND_ROOT=./ Scripts/TCOS.chroot Filesystem $(SHELL) < Scripts/TCOS.nvidia_test

qemu-stamp:update-stamp
	@echo "[1m Target $@-stamp [0m"
	-rm -f clean-stamp
	sudo -E BIND_ROOT=./ Scripts/TCOS.chroot Filesystem $(SHELL) -c "\
		dpkg --add-architecture amd64; apt-get update ;\
		apt-get install -y --no-install-recommends $(PACKAGES_QEMU) ;\
	"
	@touch $@

busybox:
	make $@-stamp

busybox-stamp: update-stamp
        # let's integrate this stuff in target driver to prevent installing build deps twice
	@echo "[1m Target busybox-stamp: Create the busybox[0m"
        # Patch for fbsplash: https://gitlab.c3sl.ufpr.br/cdn/tm-scripts/commit/c889d7d9df3a348dfc54e2f73aadf98ca6e025dc
	if [ ! -r Initrd/bin/busybox -o Sources/busybox.config -nt Initrd/bin/busybox ]; then\
		rm -f initrd-stamp ;\
		sudo rm -rf Initrd/* ;\
		cd Sources &&\
		[ -d busybox ] && mv busybox busybox_`date +%s` ;\
		git clone --depth 1 --branch $(BUSYBOX_BRANCH) git://git.busybox.net/busybox &&\
		cp busybox.config busybox/.config &&\
		patch ./busybox/miscutils/fbsplash.c fbsplash-center.patch &&\
		cd .. &&\
		PATH=$(PATH) sudo BIND_ROOT=./ Scripts/TCOS.chroot Filesystem_dkms_32 $(SHELL) -c "\
			set -e ;\
			cd /TCOS/Sources/busybox ;\
			make install ;\
		" || exit 1 ;\
	fi
	@touch $@

driver:
	make $@-stamp

driver-stamp: kernel-stamp update-stamp
	-rm -f compressed-stamp
	echo "Target driver-stamp: Compile external modules for the kernel[0m"

        # remove old existing Drivers, just to build some new
	sudo rm -rf Driver && mkdir -p Driver/lib/modules

        # Don't run with AUFS=1 to keep gcc, kernel-headers and etc. for further build scripts
	sudo -E BIND_ROOT=./ Scripts/TCOS.chroot Filesystem_dkms_32 $(SHELL)  < Scripts/TCOS.driver.dkms
	sudo -E BIND_ROOT=./ Scripts/TCOS.chroot Filesystem_dkms_64 $(SHELL)  < Scripts/TCOS.driver.dkms

	@echo
	@echo "[1m Building usbrdr for $(TARGET_KERNEL_DEFAULT)[0m"
	sudo -E AUFS=1 DST_KERNEL=$(TARGET_KERNEL_DEFAULT) BIND_ROOT=./ Scripts/TCOS.chroot Filesystem_dkms_32 $(SHELL)  < Scripts/TCOS.driver.usbrdr
	@echo "[1m Building usbrdr for $(TARGET_KERNEL_64)[0m"
	sudo -E AUFS=1 DST_KERNEL=$(TARGET_KERNEL_64)      BIND_ROOT=./ Scripts/TCOS.chroot Filesystem_dkms_64 $(SHELL)  < Scripts/TCOS.driver.usbrdr

        #Copy modules and binaries back to Filesystem	
	sudo rsync -va Driver/* Filesystem/

        #Renew module dependencies.
        #
	sudo -E BIND_ROOT=./ Scripts/TCOS.chroot Filesystem $(SHELL) -c "\
                for kernel in $(TARGET_KERNEL); do depmod \$$kernel; done \
        "
	@touch $@

initrd:
	make $@-stamp

initrd-stamp:busybox-stamp kernel-stamp driver-stamp Sources/modules.list Sources/busybox_apply_init/init Sources/busybox_apply_init/init_functions
	@echo "[1m Target $@: extract modules and compress initrd[0m"
        # Keep in mind: The folder Initrd contains the buysbox stuff already in this moment.

        # Let's have a fresh copy of Initrd/devices 
	sudo rm -rf Initrd/devices

        # Extract modules and firmware files pathnames we need when initrd starts up
        #
	sudo -E DEST_DIR=/TCOS/Initrd_modules_vmlinuz_non-pae  KERNEL=$(TARGET_KERNEL_NONPAE)  AUFS=1 BIND_ROOT=./ Scripts/TCOS.chroot Filesystem_dkms_32 $(SHELL) < Scripts/TCOS.copy_modules
	sudo -E DEST_DIR=/TCOS/Initrd_modules_vmlinuz_64       KERNEL=$(TARGET_KERNEL_64)      AUFS=1 BIND_ROOT=./ Scripts/TCOS.chroot Filesystem_dkms_32 $(SHELL) < Scripts/TCOS.copy_modules
	sudo -E DEST_DIR=/TCOS/Initrd_modules_vmlinuz          KERNEL=$(TARGET_KERNEL_DEFAULT) AUFS=1 BIND_ROOT=./ Scripts/TCOS.chroot Filesystem_dkms_32 $(SHELL) < Scripts/TCOS.copy_modules

        # extract firmware files and copy them over in each of the Intrd_module folders
        # 
	sudo rm -rf Initrd_firmware/* 
	sudo $(SHELL) -e -c 'cd Filesystem_dkms_32/lib/firmware/; rsync -vaR $$(cat ../../../Initrd_modules_vmlinuz*/firmware.list | sort -u) ../../../Initrd_firmware/ || true'
	sudo cp -al Initrd_firmware/* Initrd_modules_vmlinuz/lib/firmware/
	sudo cp -al Initrd_firmware/* Initrd_modules_vmlinuz_non-pae/lib/firmware/
	sudo cp -al Initrd_firmware/* Initrd_modules_vmlinuz_64/lib/firmware/

        # copy some essential stuff into initrd folder
	sudo $(SHELL) -e -c 'rsync -a Sources/busybox_apply_*/* Initrd/'
	sudo touch Initrd/etc/fstab

        # build/compress all the initrd-files we need
	sudo $(SHELL) -e -c 'cd Initrd && find . | fakeroot cpio -H newc -o | \
		lzop -9 - > $$OLDPWD/Base/base-$(BASE_VERSION)/debian/base/tftp/initrd.img; cd ..'
	sudo $(SHELL) -e -c 'cd Initrd_modules_vmlinuz  && find . | fakeroot cpio -H newc -o | \
		lzop -9 - > $$OLDPWD/Base/base-$(BASE_VERSION)/debian/base/tftp/vmlinuz-modules.img; cd ..'
	sudo $(SHELL) -e -c 'cd Initrd_modules_vmlinuz_non-pae  && find . |fakeroot cpio -H newc -o | \
		lzop -9 - > $$OLDPWD/Base/base-$(BASE_VERSION)/debian/base/tftp/vmlinuz_non-pae-modules.img; cd ..'
	sudo $(SHELL) -e -c 'cd Initrd_modules_vmlinuz_64  && find . | fakeroot cpio -H newc -o | \
		lzop -9 - > $$OLDPWD/Base/base-$(BASE_VERSION)/debian/base/tftp/vmlinuz_64-modules.img; cd ..'
	@touch $@


init-test:
	sudo cp -ra Sources/tftp/* Base/base-$(BASE_VERSION)/tftp/
	sudo rm -rf Initrd/devices
	sudo rsync -va Sources/busybox_apply_*/* Initrd/
	sudo touch Initrd/etc/fstab
        # Read this about compression advantages and disadvantage
        # https://catchchallenger.first-world.info/wiki/Quick_Benchmark:_Gzip_vs_Bzip2_vs_LZMA_vs_XZ_vs_LZ4_vs_LZO
	sudo $(SHELL) -e -c 'cd Initrd && find . | fakeroot cpio -H newc -ov | lzop -1 - >  $$OLDPWD/Base/base-$(BASE_VERSION)/debian/base/tftp/initrd.img; cd ..'
        #-rsync -va Base/base-$(BASE_VERSION)/tftp/initrd.img  Base/base-$(BASE_VERSION)/tftp/*modules.img $(LOCAL_TEST_PATH)/tftp/
	rsync -va Base/base-$(BASE_VERSION)/tftp/*  $(LOCAL_TEST_PATH)/tftp/



clean:
	make $@-stamp

clean-stamp: initrd-stamp
	@echo "[1m Target clean-stamp: Clean up the filesystem[0m"
        # Delete useless packages here.
        #    moved to TCOS.tcosify-clean

        # Disable/mask services and loop devices
	sudo -E BIND_ROOT=./ Scripts/TCOS.chroot Filesystem $(SHELL) -c "\
		set -e ;\
		for SERVICE in $(DISABLED_SERVICES); do systemctl disable \$$SERVICE; done ;\
		for SERVICE in $(MASKED_SERVICES); do systemctl mask \$$SERVICE; done ;\
		for bla in \$$(seq 0 30); do systemctl mask dev-loop\$${bla}.device; done ;\
	"
	-sudo -E Scripts/TCOS.chroot Filesystem $(SHELL) < Scripts/TCOS.tcosify-clean
	@touch $@

compressed:
	make $@-stamp

compressed-stamp:clean-stamp
	@echo "[1m Target compressed-stamp: Create the base.sfs container[0m"
	sudo mv Filesystem/etc/apt/sources.list.d/stretch.list Filesystem/stretch.list.moved
	sudo cp Filesystem/etc/apt/sources.list.d/public/stretch.list Filesystem/etc/apt/sources.list.d/stretch.list
        #nice -n +19 ionice -c 3 sudo 
	sudo \
		mksquashfs Filesystem Base/base-$(BASE_VERSION)/sfs/base.sfs -ef \
			Sources/mksquashfs.excludes -wildcards -noappend -always-use-fragments -comp lzo -Xcompression-level 9
	sudo mv Filesystem/stretch.list.moved Filesystem/etc/apt/sources.list.d/stretch.list
	@touch $@

# install-targets
base-release: build-with-log-stamp
	@echo "[1m Target base-release. Creating the deb-package for delivery.[0m"
	sudo cp -ra Sources/tftp/* Base/base-$(BASE_VERSION)/tftp/
	sudo -E AUFS=1 BIND_ROOT=./ Scripts/TCOS.chroot Filesystem_dkms_32 $(SHELL) -c " \
            DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
		dpkg-dev fakeroot gcc build-essential debhelper ;\
            cd TCOS/Base/base-$(BASE_VERSION) ;\
	    touch debian/files ;\
	    echo 5 > debian/compat ;\
	    echo \"Please have your comments to changelog already done.\" ;\
	    cp debian/changelog ../base.changelog ;\
	    cp debian/changelog sfs/base.changelog ;\
	    dpkg-buildpackage -r\"fakeroot -u\" -us -uc -a i386 -b -Zgzip -z2 ;\
	"
	touch $@-stamp

upload-test: compressed-stamp
	@echo "[1m Target test: Copy base.sfs, kernel, etc. to development server for testing.[0m"
	-rsync -vaP Sources/tftp/*  $(LOCAL_TEST_PATH)/tftp/
	-rsync -vaP Base/base-$(BASE_VERSION)/tftp/*  Base/base-$(BASE_VERSION)/tftp/initrd* $(LOCAL_TEST_PATH)/tftp/
	-rsync -vaP Base/base-$(BASE_VERSION)/sfs/*.sfs     $(LOCAL_TEST_PATH)/sfs/
        -rsync -vaP --exclude='.empty' Base/base-$(BASE_VERSION)/custom/*      $(LOCAL_TEST_PATH)/custom/

dist-clean:
	-sudo rm -rf Filesystem
	-sudo rm -rf Filesystem_dkms_64
	-sudo rm -rf Filesystem_dkms_32
	-sudo rm -rf Initrd_firmware
	-sudo rm -rf Driver
	-sudo rm -rf Kernel
	-sudo rm -rf Base/base-$(BASE_VERSION)/sfs/*
	-sudo rm -rf Base/base-$(BASE_VERSION)/tftp/*
	-sudo rm -f *-stamp

build-with-log:
	make dist-clean
	-rm -f base-build-$(BASE_VERSION)$(BASE_VERSION_MINOR).log
	date +%s > .time_$@.txt
	echo "Starting build at " $$(date) | tee -a base-build-$(BASE_VERSION)$(BASE_VERSION_MINOR).log
	make compressed 2>&1 | tee base-build-$(BASE_VERSION).$(BASE_VERSION_MINOR).log
	echo "Buildtime" $$(($$(($$(date +%s)-$$(cat .time_$@.txt)))/60)) " minutes." >> base-build-$(BASE_VERSION)$(BASE_VERSION_MINOR).log
	sudo cp base-build-$(BASE_VERSION).$(BASE_VERSION_MINOR).log Base/base-$(BASE_VERSION)/sfs/
	rm .time_$@.txt
	@touch $@-stamp

